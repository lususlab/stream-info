Apple HLS
  1. m3u8 inside m3u8
    -- Root m3u8:
      -- http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8
    -- Child m3u8: 
      -- http://devimages.apple.com/iphone/samples/bipbop/bipbopall/gear1/prog_index.m3u8
        -- TS Files
          -- http://devimages.apple.com/iphone/samples/bipbop/gear1/fileSequence0.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear1/fileSequence1.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear1/fileSequence2.ts

      -- http://devimages.apple.com/iphone/samples/bipbop/bipbopall/gear2/prog_index.m3u8
        -- TS Files
          -- http://devimages.apple.com/iphone/samples/bipbop/gear2/fileSequence0.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear2/fileSequence1.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear2/fileSequence2.ts

      -- http://devimages.apple.com/iphone/samples/bipbop/bipbopall/gear3/prog_index.m3u8
        -- TS Files
          -- http://devimages.apple.com/iphone/samples/bipbop/gear3/fileSequence0.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear3/fileSequence1.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear3/fileSequence2.ts

      -- http://devimages.apple.com/iphone/samples/bipbop/bipbopall/gear4/prog_index.m3u8
        -- TS Files
          -- http://devimages.apple.com/iphone/samples/bipbop/gear4/fileSequence0.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear4/fileSequence1.ts
          -- http://devimages.apple.com/iphone/samples/bipbop/gear4/fileSequence2.ts


OMG HLS
  1. Single m3u8
    -- Root m3u8:
      -- http://52.220.94.66:44436/vod/kcjJvvvb99rE/default/hls/0/kcjJvvvb99rE_520p.m3u8
        --TS Files
          -- http://52.220.94.66:44436/vod/kcjJvvvb99rE/default/hls/0/kcjJvvvb99rE_520p1.ts?ocdnSessionId=vjtEf116ZReE6L08&signature=e3b39f4b2cd3eee6b91eea5aa31afba0b25bad092cafd9bad2caea8a71b31023
          -- http://52.220.94.66:44436/vod/kcjJvvvb99rE/default/hls/0/kcjJvvvb99rE_520p2.ts?ocdnSessionId=vjtEf116ZReE6L08&signature=e3b39f4b2cd3eee6b91eea5aa31afba0b25bad092cafd9bad2caea8a71b31023
          -- http://52.220.94.66:44436/vod/kcjJvvvb99rE/default/hls/0/kcjJvvvb99rE_520p3.ts?ocdnSessionId=vjtEf116ZReE6L08&signature=e3b39f4b2cd3eee6b91eea5aa31afba0b25bad092cafd9bad2caea8a71b31023